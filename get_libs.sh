#!/bin/sh

# usage: export_git_revision(repo, rev, export_directory)
export_git_revision() {
    REPO=$1
    REV=$2
    EXPORT=$3
    if [ -z $REPO ]; then
        echo "No repository specified"
        exit 1
    fi
    if [ -z $REV ]; then
        echo "No revision specified"
        exit 1
    fi
    if [ -z $EXPORT ]; then
        echo "No export directory specified"
        exit 1
    fi
    vcs-tool --repository ${REPO} --rev ${REV} --export ${EXPORT}
}

export_git_revision "https://github.com/bartlibert/GjsUnit.git" "8749de3bb6683e4d70ca8917ab2c78b2a6be6267" "$(pwd)/third_party/gjsunit"
export_git_revision "https://github.com/jfd/optparse-js.git" "e707fa8a3e3ed96b263c5352ad15578196a68cbe" "$(pwd)/third_party/optparse"
